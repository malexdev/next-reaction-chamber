# reaction-chamber
React state manager

# goals

- Stores instantiated by key. Components on disparate pages can access the same store via key.
- Automatically wrap components in a store without having to manually convert to props.
- Merge typical web state storage locations (like URL params) into the state object (Planned).

# why not redux/reflux/etc/...?

Redux et al require the app to be constructed a certain way.
The primary goal of this project is to reduce the amount of boilerplate needed by anticipating common use cases, 
while still giving the developer power over their app.

# import

Import ReactionChamber with `import ReactionChamber from 'next-reaction-chamber'`.

# api

To wrap your component in a ReactionChamber, create a `ReactionChamber` component with an `id` key.

Additionally, if desired, provide an initial state for the store via `initialState`:

````jsx
<ReactionChamber id={'my-store-id'} initialState={ /* initial state object, defaults to {} if not passed */ }>
  { /* sub component */ }
</ReactionChamber>
````

Do note that the `id` key allows ReactionChambers to be shared across pages by using the same `id`.

# async initial state

The simplest way to handle async initial state is with Next.js-powered `getInitialProps`.

You can then pass the initial state to your ReactionChamber instance.

````jsx
static async getInitialProps() {
  return {
    initialState: { myInitialState: 0 }
  }
}

render() {
  return (
    <div>
      <ReactionChamber id={'my-store-id'} initialState={this.props.initialState}>
        { /* sub component */ }
      </ReactionChamber>
    </div>
  )
}
````

# accept ReactionChamber props

ReactionChamber will pass two props to its direct children:

property | description
--- | ---
`store` | This is the ReactionChamber state.
`setStore` | This method works very similar to React's own `setState`, it just sets the state for ReactionChamber.

Example component:

````jsx
const Demo = ({ setStore, store: { count } }) => {
  return (
    <div>
      <h1>next-reaction-chamber demo component</h1>
      <div>Clicks: {count}</div><br/>
      <button onClick={() => setStore({ count: count + 1 })}>Increment</button>
    </div>
  )
}
````