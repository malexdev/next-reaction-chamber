'use strict'

import EventEmitter from 'events'
import isFunction from 'lodash.isfunction'
import assign from './assign'

export default class Store extends EventEmitter {

  constructor(id, initialState, options) {
    super()

    if (!id) { throw new Error('ID for store must be specified') }

    this.id = id
    this.state = assign(initialState)
    this.options = assign(options)

    this.setState = this.setState.bind(this)
    this.get = this.get.bind(this)
  }

  setState(diff) {
    // ensure that the diff is actually different
    if (keysAreSame(this.state, diff)) { return }

    // create a new state object with the diff and assign it to the state
    this.state = assign(this.state, diff)
    this.emit('changed', this.state)
  }

  get() {
    return assign(this.state)
  }

  static InitialProps(fn) {
    return Promise.resolve()
    .then(() => isFunction(fn) ? fn() : fn)
    .then(result => {
      return { initialState: result }
    })
  }
}

function keysAreSame(obj = {}, newObj = {}) {
  const keys = Object.keys(newObj)
  return keys.length ? keys.every(key => newObj[key] === obj[key]) : false
}