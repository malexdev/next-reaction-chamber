'use strict'

import React from 'react'
import Store from './Store'
import assign from './assign'
import { create } from './storehouse'

export default class ReactionChamber extends React.Component {

  static InitialProps(...args) {
    return Store.InitialProps(...args)
  }

  constructor(props) {
    super(props)

    const { id, initialState, options } = props
    const store = create(id, initialState, options)

    this.state = { store, storeState: store.get() }
    this.storeUpdated = this.storeUpdated.bind(this)
  }

  componentDidMount() {
    this.state.store.on('changed', this.storeUpdated)
  }

  componentWillUnmount() {
    this.state.store.removeListener('changed', this.storeUpdated)
  }

  storeUpdated(newState) {
    this.setState({ storeState: newState })
  }

  render() {
    return (
      <div>
      {
        React.Children.map(this.props.children, child => {
          return React.cloneElement(child, assign({ store: this.state.storeState }, { setStore: this.state.store.setState }))
        })
      }
      </div>
    )
  }
}