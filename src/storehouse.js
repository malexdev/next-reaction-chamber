'use strict'

import Store from './Store'

const storehouse = {}

export function create(id, initialState = {}, options = {}) {
  if (storehouse[id]) {
    return storehouse[id]
  }

  storehouse[id] = new Store(id, initialState, options)
  return storehouse[id]
}