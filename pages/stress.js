import React from 'react'

import ReactionChamber from '../src'
import Demo from './_demo'
import Nav from './_nav'

const id = 'index-stress'

export default class Index extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      itemCount: 100
    }
  }

  static async getInitialProps() {
    await new Promise(resolve => setTimeout(resolve, 10))
    return await ReactionChamber.InitialProps({ count: 0 })
  }

  render() {
    const items = []

    for (let i = 0; i < this.state.itemCount; i++) {
      items.push(
        <ReactionChamber key={i} id={id} initialState={this.props.initialState}>
          <Demo id={`item ${i + 1}`} />
        </ReactionChamber>
      )
    }
    
    return (
      <div>
        <Nav />
        <hr/>
        <span>Number of items: </span>
        <input 
          type='number' 
          name='item count' 
          min='1' 
          max='10000' 
          value={this.state.itemCount} 
          onChange={e => this.setState({ itemCount: e.target.value })} 
        />
        <hr/>
        { items }
      </div>
    )
  }
}