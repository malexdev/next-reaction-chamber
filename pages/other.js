import React from 'react'

import ReactionChamber from '../src'
import Demo from './_demo'
import Nav from './_nav'

export default class Index extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  static async getInitialProps() {
    await new Promise(resolve => setTimeout(resolve, 10))
    return await ReactionChamber.InitialProps({ count: 0 })
  }

  render() {
    return (
      <div>

        <Nav />
        <hr/>

        <ReactionChamber id={'index'} initialState={this.props.initialState}>
          <Demo id='index' />
        </ReactionChamber>
      </div>
    )
  }
}