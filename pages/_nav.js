
import Link from 'next/link'

export default () => (
  <div>
    <div>Page navigation:</div>
    <Link href='/'>index</Link> | <Link href='/other'>other</Link> | <Link href='/stress'>stress test</Link>
  </div>
)