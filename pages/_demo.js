export default ({ setStore, store: { count }, id }) => {
  return (
    <div>
      <h1>next-reaction-chamber demo instance #{id}</h1>
      <div>Clicks: {count}</div><br/>
      <button onClick={() => setStore({ count: count + 1 })}>Increment</button>
    </div>
  )
}